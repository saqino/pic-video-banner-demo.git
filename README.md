# PicVideoBannerDemo

#### 介绍
基于https://github.com/youth5201314/banner 扩展，图片+视频无限轮播banner的demo


#### 使用说明

1.  克隆包到本地
2.  查看demo
3.  banner包是核心内容

有看到有些朋友提出可能存在内存泄露的问题，我这边用demo使用的固定的图片视频内容测试过，好像没发现，具体问题可以提一个issue，最好说明banner使用的数据类型、使用环境、内存泄露可复现操作，不然我这边也没法处理